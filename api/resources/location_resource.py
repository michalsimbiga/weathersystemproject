from flask import jsonify
from flask_restful import Resource, reqparse
from wsapp.services import weather as WS

parser = reqparse.RequestParser()
parser.add_argument('name', help='Name of the city', required=True)
parser.add_argument('city_key', help='City key used by accuweather')

class LocationResource(Resource):
    """
    Resource for specific location
    """
    def get(self):
        """
        Uses weather service to resolve location by name
        """
        data = parser.parse_args()
        model = WS.get_location_by_name(loc_name=data['name'])

        if model is None:
            return {'message':'city not found'}

        return {'id':model.id, 'name':model.name, 'city key':model.city_key}

    def post(self):
        """
        Uses weather service to create new location from imput arguments
        """
        data = parser.parse_args()

        if not data['city_key']:
            return {'message': 'Please provide city key for location'}
        model = WS.create_new_location(loc_name=data['name'], city_key=data['city_key'])

        return jsonify({'city': {'id': model.id, 'name':model.name, 'city_key':model.city_key}})


class LocationsResource(Resource):
    """
    Resource for all of the locations
    """
    def get(self):
        """
        Uses weather service to get a list of all the cities in database
        """
        models = WS.get_all_locations()

        cities = []
        for model in models:
            cities.append({'id':model.id, 'name':model.name, 'city_key':model.city_key})

        return jsonify({'cities': cities})
