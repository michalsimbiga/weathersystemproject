from datetime import datetime
from flask_restful import Resource
from flask_jwt_extended import create_access_token

class JWTResource(Resource):
    """
    Endpoint for token generation
    """
    def get(self):
        """
        Generates token on request
        """
        token = create_access_token(identity=datetime.now().date())

        return {'token': token}
