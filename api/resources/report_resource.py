from datetime import datetime
from flask import jsonify
from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required
from wsapp.services import weather as WS

parser = reqparse.RequestParser()
parser.add_argument('name', help='Name of the city', required=True)
parser.add_argument('date', help='Date for the report')
parser.add_argument('temp_min', help='Minimal temeprature that day')
parser.add_argument('temp_max', help='Maximal temeprature that day')
parser.add_argument('summary', help='Summary for weather that day')


class ReportResource(Resource):
    """ Resource for specific raport """
    def get(self):
        """
        Returns list of reports for given location name
        """
        data = parser.parse_args()

        reports = WS.get_reports_for_location_by_location_name(loc_name=data['name'])

        list_of_reports = []
        for report in reports:
            list_of_reports.append({'location': report.location.name,
                                    'date': report.date.strftime('%Y-%m-%d'),
                                    'min temp': report.temp_min,
                                    'max temp': report.temp_max,
                                    'summary': report.summary,
                                    })

        return jsonify({'reports': list_of_reports})

    @jwt_required
    def post(self):
        """
        Creates a new report from given parameters
        """
        data = parser.parse_args()

        if (not data['date']) or (not data['temp_min']) or \
           (not data['temp_max']) or (not data['summary']):
            return {'message': 'Please provide all parameters for creating a weather report'}

        model = WS.create_weather_for_location(loc_name=data['name'],
                                               date=datetime.strptime(data['date'], '%Y-%m-%d'),
                                               temp_min=data['temp_min'],
                                               temp_max=data['temp_max'],
                                               summary=data['summary'])

        return {'message': 'Report created successfully',
                'location' : {'name': model.location.name,
                              'date': model.date,
                              'temp min': model.temp_min,
                              'temp max': model.temp_max,
                              'summary': model.summary
                              }
                }
