from flask_restful import Api
from api.resources.location_resource import LocationResource, LocationsResource
from api.resources.report_resource import ReportResource
from api.resources.jwt_resource import JWTResource
from flask_jwt_extended import JWTManager

def init_api(app):
    """ Initializes application with api resources """

    jwt = JWTManager(app)

    api = Api(app, prefix="/api")
    api.add_resource(LocationResource, '/city')
    api.add_resource(LocationsResource, '/cities')
    api.add_resource(ReportResource, '/report')
    api.add_resource(JWTResource, '/auth')
