from datetime import date
from wsapp.models.location import Location
from wsapp.models.report import Report

# Location Repository
def test_location_repository_create_location_method(loc_repo, loc_opole, db):
    model = loc_repo.create_location(name=loc_opole.name, city_key=loc_opole.city_key)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_location_repository_create_location_method_while_model_exists(loc_repo, loc_opole, db):
    loc_repo.create_location(name=loc_opole.name, city_key=loc_opole.city_key)
    loc_repo.create_location(name=loc_opole.name, city_key=loc_opole.city_key)
    list_models = db.session.query(Location).all()

    assert len(list_models) == 1

def test_base_repository_add_inherited_method_persistence(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    per_model = db.session.query(Location).filter_by(name='Opole').first()

    assert per_model is not None
    assert per_model.name == loc_opole.name
    assert per_model.city_key == loc_opole.city_key

def test_location_repository_update_location_method(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    model = db.session.query(Location).filter_by(name='Opole').first()
    loc_repo.update_location(model, name='Test', city_key=112233)
    upd_model = db.session.query(Location).filter_by(name='Test').first()

    assert upd_model is not None
    assert upd_model.name == 'Test'
    assert upd_model.city_key == 112233

def test_location_repository_find_all_method(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    loc_repo.add(Location(name='Test', city_key=11111))

    models = loc_repo.find_all()

    assert len(models) == 2

def test_location_repository_find_all_method_no_results(loc_repo, loc_opole, db):
    models = loc_repo.find_all()

    assert models == []

def test_location_repository_find_location_by_name_method(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    model = loc_repo.find_location_by_name('Opole')

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_location_repository_find_location_by_name_method_model_not_exist(loc_repo, loc_opole, db):
    model = loc_repo.find_location_by_name('Opole')

    assert model is None

def test_location_repository_find_location_by_id_method(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    model = loc_repo.find_location_by_id(id=loc_opole.id)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_location_repository_find_location_by_id_method_model_not_exist(loc_repo, loc_opole, db):
    model = loc_repo.find_location_by_id(id=loc_opole.id)

    assert model is None

def test_location_repository_find_location_by_city_key_method(loc_repo, loc_opole, db):
    loc_repo.add(loc_opole)
    model = loc_repo.find_location_by_city_key(city_key=loc_opole.city_key)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_location_repository_find_location_by_city_key_method_model_not_exist(loc_repo, loc_opole, db):
    model = loc_repo.find_location_by_id(id=loc_opole.id)

    assert model is None


# Report Repository
def test_report_repository_create_report_method(report_repo, loc_opole, rep_opole, db):
    report = report_repo.create_report(loc_model=loc_opole, date=rep_opole.date,
                                       temp_min=rep_opole.temp_min, temp_max=rep_opole.temp_max,
                                       summary=rep_opole.summary)

    assert report is not None
    assert report.location == loc_opole
    assert report.date == rep_opole.date
    assert report.temp_min == rep_opole.temp_min
    assert report.temp_max == rep_opole.temp_max
    assert report.summary == rep_opole.summary

def test_report_repository_inherited_base_add_method(report_repo, loc_opole, rep_opole, db):
    report_repo.add(rep_opole)
    report = db.session.query(Report).filter_by(location=loc_opole).first()

    assert report is not None
    assert report.location == loc_opole
    assert report.date == rep_opole.date
    assert report.temp_min == rep_opole.temp_min
    assert report.temp_max == rep_opole.temp_max
    assert report.summary == rep_opole.summary

def test_report_repository_update_report_method(report_repo, loc_opole, rep_opole, db):
    report_repo.add(rep_opole)
    p_date = date(2018, 12, 1)
    per_model = db.session.query(Report).filter_by(location=loc_opole).first()
    report_repo.update_report(report_model=per_model, new_date=p_date,
                              new_t_min=-5, new_t_max=10, new_summary='TestTest')
    report = db.session.query(Report).filter_by(date=p_date).first()

    assert report is not None
    assert report.location == loc_opole
    assert report.date == p_date
    assert report.temp_max == 10
    assert report.temp_min == -5
    assert report.summary == 'TestTest'

def test_report_repository_find_reports_by_location_method(report_repo, loc_opole, rep_opole, db):
    report_repo.add(rep_opole)
    reports = report_repo.find_reports_by_location(loc_opole)

    assert len(reports) == 1
    assert reports[0] == rep_opole

def test_report_repository_find_reports_by_location_method_reports_dont_exist(report_repo, loc_opole, rep_opole, db):
    reports = report_repo.find_reports_by_location(loc_opole)

    assert reports == []

def test_report_repository_find_reports_by_date_method(report_repo, loc_opole, rep_opole, db):
    report_repo.add(rep_opole)
    reports = report_repo.find_reports_by_date(rep_opole.date)

    assert len(reports) == 1
    assert reports[0] == rep_opole

def test_report_repository_find_reports_by_date_method_reports_dont_exist(report_repo, loc_opole, rep_opole, db):
    reports = report_repo.find_reports_by_location(loc_opole)

    assert reports == []

def test_report_repository_find_reports_by_location_and_date_method(report_repo, loc_opole, rep_opole, db):
    report_repo.add(rep_opole)
    reports = report_repo.find_reports_by_location_and_date(loc_model=loc_opole,
                                                            date=rep_opole.date)

    assert len(reports) == 1
    assert reports[0] == rep_opole

def test_report_repository_find_reports_by_location_and_date_method_reports_dont_exist(report_repo, loc_opole, rep_opole, db):
    reports = report_repo.find_reports_by_location_and_date(loc_model=loc_opole,
                                                            date=rep_opole.date)

    assert reports == []
