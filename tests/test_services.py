from datetime import datetime, date
from wsapp.services import weather as WS
from wsapp.models.report import Report
from wsapp.models.location import Location

def test_ws_create_new_location(loc_opole, db):
    model = WS.create_new_location(loc_name=loc_opole.name, city_key=loc_opole.city_key)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_ws_create_new_location_while_location_exists(loc_opole, populated_db):
    model = WS.create_new_location(loc_name=loc_opole.name, city_key=loc_opole.city_key)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

def test_ws_create_weather_for_location(loc_opole, rep_opole, db):
    db.session.add(loc_opole)
    db.session.commit()
    model = WS.create_weather_for_location(loc_name=loc_opole.name, date=rep_opole.date,
                                           temp_max=rep_opole.temp_max, temp_min=rep_opole.temp_min,
                                           summary=rep_opole.summary)
    assert model is not None

    db_rep_model = Report.query.filter_by(location=loc_opole).all()

    assert db_rep_model != []
    assert db_rep_model[0].location == loc_opole
    assert db_rep_model[0].date == rep_opole.date
    assert db_rep_model[0].temp_min == rep_opole.temp_min
    assert db_rep_model[0].temp_max == rep_opole.temp_max
    assert db_rep_model[0].summary == rep_opole.summary

def test_ws_get_all_locations(loc_test, populated_db):
    populated_db.session.add(loc_test)
    populated_db.session.commit()
    locations = WS.get_all_locations()

    assert len(locations) == 2

def test_ws_get_all_locations_no_location_exist(db):
    locations = WS.get_all_locations()

    assert locations == []

def test_ws_get_location_by_name(loc_opole, db):
    db.session.add(loc_opole)
    db.session.commit()

    model = WS.get_location_by_name(loc_name=loc_opole.name)

    assert model is not None
    assert model.name == loc_opole.name
    assert model.city_key == loc_opole.city_key

# Uses ACUAPI to resolve name to city key, commented for saving up on API connections rate limit
#
# def test_ws_get_location_by_name_location_not_in_db(loc_opole, db):
#
#     model = WS.get_location_by_name(loc_name=loc_opole.name)
#
#     assert model is not None
#     assert model.name == loc_opole.name
#     assert model.city_key == loc_opole.city_key
#
#     dbmodel = Location.query.filter_by(name=loc_opole.name).first()
#
#     assert dbmodel is not None
#     assert dbmodel.name == loc_opole.name
#     assert dbmodel.city_key == loc_opole.city_key

def test_ws_get_report_for_location_by_location_name(loc_opole, rep_opole, populated_db):
    reports = WS.get_reports_for_location_by_location_name(loc_name=loc_opole.name)

    assert reports != []
    assert reports[0] == rep_opole

def test_ws_get_report_for_location_by_location_name_location_and_city_dont_exist(loc_opole, db):
    reports = WS.get_reports_for_location_by_location_name(loc_name=loc_opole.name)

    assert reports == []

def test_ws_get_report_for_location_by_location_city_key(loc_opole, rep_opole, populated_db):
    reports = WS.get_reports_for_location_by_location_city_key(city_key=loc_opole.city_key)

    assert reports != []
    assert reports[0] == rep_opole

def test_ws_get_report_for_location_by_location_city_key_report_and_city_dont_exist(loc_opole, rep_opole, db):
    reports = WS.get_reports_for_location_by_location_city_key(city_key=loc_opole.city_key)

    assert reports == []

def test_ws_get_reports_by_daterange_and_location_test_for_default_days(loc_opole, populated_db):
    reports = WS.get_reports_by_daterange_and_location(loc_name=loc_opole.name,
                                                       date_from='', date_to='')

    assert len(reports) == 3
    assert reports[0].date == datetime.now().date()

def test_ws_get_report_by_daterange_and_location_test_for_current_day(loc_opole, populated_db):
    current = datetime.now().date()
    current_str = current.strftime('%Y-%m-%d')
    reports = WS.get_reports_by_daterange_and_location(loc_name=loc_opole.name,
                                                       date_from='', date_to=current_str)

    assert len(reports) == 1
    assert reports[0].date == current

def test_ws_delete_location_by_name(loc_opole, db):
    db.session.add(loc_opole)
    db.session.commit()
    WS.delete_location_by_name(loc_name=loc_opole.name)

    model = Location.query.filter_by(name=loc_opole.name).first()

    assert model is None

def test_ws_delete_repor_by_location_name_and_date(loc_opole, rep_opole, populated_db):
    WS.delete_report_by_location_name_and_date(loc_name=loc_opole.name, date=rep_opole.date)

    model = Report.query.filter_by(location=loc_opole, date=rep_opole.date).first()

    assert model is None
