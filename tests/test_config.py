from wsapp.config import conf_by_name

def test_dev_config(app):
    """ Test if the developement config loads correctly """
    app.config.from_object(conf_by_name['dev'])

    assert app.config['DEBUG']
    assert not app.config['TESTING']
    assert not app.config['SQLALCHEMY_TRACK_MODIFICATIONS']

def test_test_config(app):
    """ Test if the test config loads correctly """
    app.config.from_object(conf_by_name['test'])

    assert app.config['DEBUG']
    assert app.config['TESTING']
    assert not app.config['SQLALCHEMY_TRACK_MODIFICATIONS']

def test_prod_config(app):
    """ Test if the developement config loads correctly """
    app.config.from_object(conf_by_name['prod'])

    assert not app.config['DEBUG']
    assert not app.config['TESTING']
    assert not app.config['SQLALCHEMY_TRACK_MODIFICATIONS']
