from datetime import datetime
import pytest
from wsapp import create_app
from wsapp.models import db as _db
from wsapp.repositories.location_repository import LocationRepository as LRep
from wsapp.repositories.report_repository import ReportRepository as RRep
from wsapp.models.location import Location
from wsapp.models.report import Report
from wsapp.controllers.ws_controller import WSController as WSC

@pytest.fixture(scope='function')
def app():
    """ Returns flask app instance """
    application = create_app()
    ctx = application.app_context()
    ctx.push()
    yield application
    ctx.pop()

@pytest.fixture(scope='function')
def db(app):
    """ Returns db session """
    _db.app = app
    _db.create_all()
    yield _db
    _db.session.commit()
    _db.reflect()
    _db.drop_all()

@pytest.fixture(scope='function')
def populated_db(app, loc_opole, rep_opole):
    """ Returns populated db session """
    _db.app = app
    _db.create_all()
    _db.session.add(loc_opole)
    _db.session.add(rep_opole)
    _db.session.commit()
    yield _db
    _db.session.commit()
    _db.reflect()
    _db.drop_all()

@pytest.fixture
def loc_repo():
    """ Create location repository object and return its instance """
    loc_rep = LRep()
    return loc_rep

@pytest.fixture
def report_repo():
    """ Create report repository object and return its instance """
    report_rep = RRep()
    return report_rep

@pytest.fixture
def loc_test():
    """ Returns test location model """
    loc = Location(name='Test', city_key=123456)
    return loc

@pytest.fixture
def loc_opole():
    """ Returns specific test location model (opole) """
    loc = Location(name='Opole', city_key=274945)
    return loc

@pytest.fixture
def rep_opole(loc_opole):
    """ Returns test report model assigned to input location """
    rep = Report(location=loc_opole, date=datetime.now().date(),
                 temp_min=-1, temp_max=5, summary="Summary")
    return rep

@pytest.fixture
def wscontroller():
    """ Returns controller instance """
    controller = WSC()
    return controller
