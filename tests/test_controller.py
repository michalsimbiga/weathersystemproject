from datetime import datetime, date, timedelta
#   WSController returns DTO objects


def test_wsc_get_location_location_exists_in_db(loc_opole, populated_db, wscontroller):
    result = wscontroller.get_location(location=loc_opole.name)

    assert result is not None
    assert result.name == loc_opole.name
    assert result.city_key == loc_opole.city_key

#   Uses ACUAPI to resolve location city key, commented for saving up on API rate limit
# def test_wsc_get_location_location_doesnt_exist_in_db(loc_opole, db, wscontroller):
#     result = wscontroller.get_location(location=loc_opole.name)
#
#     assert result is not None
#     assert result.name == loc_opole.name
#     assert result.city_key == loc_opole.city_key


def test_wsc_get_reports_for_default_date_range(loc_opole, populated_db, wscontroller):
    results = wscontroller.get_reports(loc_name=loc_opole.name, date_from='', date_to='')

    current = datetime.now().date()

    assert len(results) == 3
    assert results[0].date == current
    assert results[1].date == current + timedelta(days=1)
    assert results[2].date == current + timedelta(days=2)

def test_wsc_get_reports_for_current(loc_opole, populated_db, wscontroller):
    current = datetime.now().date()
    current_str = current.strftime('%Y-%m-%d')
    results = wscontroller.get_reports(loc_name=loc_opole.name,
                                       date_from=current_str, date_to=current_str)

    assert len(results) == 1
    assert results[0].date == current

def test_wsc_get_reports_for_range_of_2_days(loc_opole, populated_db, wscontroller):
    current = datetime.now().date()
    current_str = current.strftime('%Y-%m-%d')
    ending = current + timedelta(days=1)
    ending_str = ending.strftime('%Y-%m-%d')

    results = wscontroller.get_reports(loc_name=loc_opole.name,
                                       date_from=current_str, date_to=ending_str)

    assert len(results) == 2
    assert results[0].date == current
    assert results[1].date == ending


def test_wsc_get_reports_for_range_of_5_days(loc_opole, populated_db, wscontroller):
    current = datetime.now().date()
    current_str = current.strftime('%Y-%m-%d')
    ending = current + timedelta(days=4)
    ending_str = ending.strftime('%Y-%m-%d')

    results = wscontroller.get_reports(loc_name=loc_opole.name,
                                       date_from=current_str, date_to=ending_str)

    assert len(results) == 5
    assert results[0].date == current
    assert results[1].date == current + timedelta(days=1)
    assert results[2].date == current + timedelta(days=2)
    assert results[3].date == current + timedelta(days=3)
    assert results[4].date == ending
