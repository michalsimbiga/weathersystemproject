from flask import current_app, render_template, request

@current_app.route('/')
def index():
    """ Renders index page """
    current_app.logger.info("Request: {}".format(request.path))
    return render_template("wsapp/index.html")
