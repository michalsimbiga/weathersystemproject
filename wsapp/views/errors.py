from flask import current_app, render_template, request

@current_app.errorhandler(404)
def not_found(error):
    current_app.logger.error('Page not found {}'.format(request.path))
    return render_template('errors/404.html')

@current_app.errorhandler(500)
def internal_server_error(error):
    current_app.logger.error('Server error: {}'.format(error))
    return render_template('errors/unhandled.html')

@current_app.errorhandler(Exception)
def unhandled_exception(error):
    current_app.logger.error('Unhandled error: {}'.format(' '.join(error.args)))
    return render_template('errors/unhandled.html')
