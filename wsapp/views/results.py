from flask import current_app, render_template, request
from wsapp.controllers.ws_controller import WSController


@current_app.route('/get-weather', methods=['POST'])
def get_weather():
    """ Renders results page with found reports for given location and dates """
    if request.method == 'POST':

        date_from = request.form['startdate']
        date_to = request.form['enddate']
        input_location = request.form['location']

        current_app.logger.info("Request: {} Params: From {} To {} Location {}".format(request.path, date_from,
                                                                                       date_to, input_location))

        wscon = WSController()

        loc_dto = wscon.get_location(input_location)
        reports_dto_list = wscon.get_reports(input_location, date_from, date_to)

        report_template = render_template('wsapp/results.html',
                                          location=loc_dto, weather_reports=reports_dto_list)
    else:
        report_template = render_template("wsapp/index.html")

    return report_template
