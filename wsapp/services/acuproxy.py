import requests
import os
from datetime import datetime
from bs4 import BeautifulSoup
from wsapp.config import BASE_DIR
from wsapp.models.report import Report

ACUKEY = open(os.path.join(BASE_DIR, 'acuapikey.txt'), 'r').read().replace('\n', '')

def get_city_key(location):
    """
    Looks up city key for provided location
    :location: String with location name
    :return: Integer with city key for given location
    """
    url = "http://dataservice.accuweather.com/locations/v1/cities/search?apikey={}&q={}"
    response = requests.get(url.format(ACUKEY, location))
    json_response = response.json()
    city_key = int(json_response[0]['Key'])
    return city_key

def get_five_day_forecast(location):
    """
    Fetches five day forecast for provied location
    :location: location database model
    :return: List with report models
    """
    url = "http://dataservice.accuweather.com/forecasts/v1/daily/5day/{}?apikey={}&metric=true"
    response = requests.get(url.format(location.city_key, ACUKEY))
    json_response = response.json()

    forecasts_list = []

    for item in json_response['DailyForecasts']:
        date = item['Date'].split('+')[0]
        date_dtime = datetime.strptime(date, '%Y-%m-%dT%H:%M:%S')
        min_temp = item['Temperature']['Minimum']['Value']
        max_temp = item['Temperature']['Maximum']['Value']
        summary = item['Day']['IconPhrase']

        forecasts_list.append(Report(location=location, date=date_dtime.date(),
                                     temp_min=min_temp, temp_max=max_temp, summary=summary))
    return forecasts_list


def get_report_for_location_by_datedelta(loc, datedelta):
    """
    Scrapes acuweather website for weather info on given day for location
    :loc: location model
    :datedelta: Integer with a number of days relative to current day (current = 1)
    :return: interger temp max, interger temp min, string summary
    """
    #   URL helper: {location} {City_key} {City_key} {day (current=1)}
    url = "https://www.accuweather.com/en/pl/{loc}/{key}/daily-weather-forecast/{key}?day={day}"

    response = requests.get(url.format(loc=loc.name, key=loc.city_key, day=datedelta),
                            headers={'User-agent': 'Chrome_scraper'})
    soup = BeautifulSoup(response.text, "html.parser")
    weather = soup.find(id='detail-day-night', attrs={'class':'detail-tab-panel'})

    temp = weather.find_all('span', attrs={'class':'large-temp'})
    temp_max = int(temp[0].text.split('°')[0].strip())
    temp_min = int(temp[1].text.split('°')[0].strip())

    cond = weather.find('div', attrs={'class':'cond'})
    summary = cond.text.strip()

    return temp_max, temp_min, summary
