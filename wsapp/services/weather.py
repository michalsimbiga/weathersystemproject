from wsapp.repositories.location_repository import LocationRepository
from wsapp.repositories.report_repository import ReportRepository
from wsapp.services import acuproxy as acu
from datetime import datetime, timedelta

loc_repo = LocationRepository()
report_repo = ReportRepository()

def create_new_location(loc_name, city_key):
    """
    Checks if location already exists, otherwise create location
    :loc_name: string
    :city_key: int
    """
    loc_model = loc_repo.find_location_by_name(loc_name)
    if loc_model is None:
        return loc_repo.create_location(loc_name, city_key)
    return loc_model

def create_weather_for_location(loc_name, date, temp_max, temp_min, summary):
    """
    Creates weather for given location and saves it do database
    :loc_name: string with location location name
    :date: string with date in Y-m-d format
    :temp_max: integer with maximum temperature
    :temp_min: integer with minimum temperature
    :summary: string with short description of weather that day
    """
    loc_model = get_location_by_name(loc_name)
    report_model = report_repo.create_report(loc_model=loc_model, date=date,
                                             temp_max=temp_max, temp_min=temp_min,
                                             summary=summary)
    return report_model

def get_all_locations():
    """
    Returns list of all locations from db
    :return: list
    """
    locations = loc_repo.find_all()
    return locations

def get_location_by_name(loc_name):
    """
    Finds location model by location name
    :loc_name: string with location name
    :return: location model
    """
    loc_model = loc_repo.find_location_by_name(loc_name)
    if loc_model is None:
        key = acu.get_city_key(loc_name)
        loc_model = loc_repo.create_location(name=loc_name, city_key=key)
    return loc_model

def get_reports_for_location_by_location_name(loc_name):
    """
    Finds reports from location by location name
    :loc_name: string with location location name
    :return: list of reports models that match location name
    """

    loc_model = loc_repo.find_location_by_name(loc_name)
    reports = report_repo.find_reports_by_location(loc_model)
    return reports

def get_reports_for_location_by_location_city_key(city_key):
    """
    Finds reports from location by location city key
    :city_key: integer with location city key
    :return: list of reports models
    """

    loc_model = loc_repo.find_location_by_city_key(city_key)
    reports = report_repo.find_reports_by_location(loc_model)
    return reports

def get_reports_by_daterange_and_location(loc_name, date_from, date_to):
    """
    Finds reports from location by date range
    :loc_name: string with location location name
    :date_from: string with date in Y-m-d format
    :date_to: string with date in Y-m-d format
    :return: list of reports models for location from range of dates
    """

    loc_model = loc_repo.find_location_by_name(loc_name)

    weather_reports = []

    datetime_date_from = (datetime.now().date()) if (date_from == '') \
                    else (datetime.strptime(date_from, '%Y-%m-%d').date())

    datetime_date_to = (datetime_date_from + timedelta(days=2)) if (date_to == '') \
                  else (datetime.strptime(date_to, '%Y-%m-%d').date())

    delta = datetime_date_to - datetime_date_from

    for i in range(delta.days + 1):
        querydate = datetime_date_from + timedelta(days=i)
        querydate_str = querydate.strftime('%Y-%m-%d')
        reports = report_repo.find_reports_by_location_and_date(loc_model, querydate_str)

        if reports == []:
            current = datetime.now().date()
            datedelta = querydate - current
            t_max, t_min, summ = acu.get_report_for_location_by_datedelta(loc_model,
                                                                          datedelta.days + 1)
            report = report_repo.create_report(loc_model=loc_model, date=querydate,
                                               temp_min=t_min, temp_max=t_max, summary=summ)
            weather_reports.append(report)
        else:
            weather_reports.extend(reports)

    return weather_reports

def delete_location_by_name(loc_name):
    """
    Deletes location from database by location name
    :loc_name: string with location name
    """

    loc_model = loc_repo.find_location_by_name(loc_name)
    if loc_model is not None:
        loc_repo.delete(loc_model)

def delete_report_by_location_name_and_date(loc_name, date):
    """
    Deletes reports by location name and date provided
    :loc_name: string with location name
    :date: string with date in format Y-m-d
    """

    loc_model = loc_repo.find_location_by_name(loc_name)
    if loc_model is not None:
        reports = report_repo.find_reports_by_location_and_date(loc_model, date)
        if reports != []:
            for report in reports:
                report_repo.delete(report)
