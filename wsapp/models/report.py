from wsapp.models import db


class Report(db.Model):
    """ Stores details of a weather report and foreign key for Location """
    id = db.Column(db.Integer, primary_key=True)
    location_id = db.Column(db.Integer, db.ForeignKey('location.id'), nullable=False)
    date = db.Column(db.Date, nullable=False)
    temp_min = db.Column(db.Float(5), nullable=False)
    temp_max = db.Column(db.Float(5), nullable=False)
    summary = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return "<Weather for Date: {} Min: {} Max: {} Summary: {}>".format(
            self.date, self.temp_min, self.temp_max, self.summary)
