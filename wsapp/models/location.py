from wsapp.models import db


class Location(db.Model):
    """ Stores location name and city_key """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    city_key = db.Column(db.Integer, nullable=False, unique=True)
    weather_reports = db.relationship('Report', backref='location')

    def __repr__(self):
        return "<Location: {}, city key:{}>".format(self.name, self.city_key)
