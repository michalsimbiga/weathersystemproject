
class LocationDTO:

    def __init__(self, name, key):
        self.name = name
        self.city_key = key

    def __repr__(self):
        return "Location: {} with city_key of: {}".format(self.name, self.city_key)
