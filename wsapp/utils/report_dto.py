
class ReportDTO:

    def __init__(self, date, temp_min, temp_max, summary):
        self.date = date
        self.temp_min = temp_min
        self.temp_max = temp_max
        self.summary = summary

    def __repr__(self):
        return "Report for {} min: {} max: {} summary: {}".format(self.date, self.temp_min,
                                                                  self.temp_max, self.summary)
