from wsapp.repositories.base_repository import BaseRepository
from wsapp.models.location import Location

class LocationRepository(BaseRepository):
    """
    Location Repository
    """
    def create_location(self, name, city_key):
        """
        Creates location model and saves it to database
        :name: string with location name
        :city_key: integer containing location city key
        :return: location model
        """
        db_model = Location(name=name, city_key=city_key)
        self.add(db_model)
        return db_model

    def update_location(self, db_model, name, city_key):
        """
        Takes in location model and updates its fields
        :db_model: database model to be updated
        :name: string with location name
        :city_key: integer containing location city key
        :return: location model
        """
        db_model.name = name
        db_model.city_key = city_key

    def find_all(self):
        """
        Returns list of all location models from database
        :return: list
        """
        locations = Location.query.all()
        return locations

    def find_location_by_name(self, name):
        """
        Searches for location model by city name
        :name: string with location name
        :return: found location model
        """
        db_model = Location.query.filter_by(name=name).first()
        return db_model

    def find_location_by_id(self, id):
        """
        Searches for location model by database ID
        :id: integer with location id
        :return: found location model
        """
        db_model = Location.query.filter_by(id=id).first()
        return db_model

    def find_location_by_city_key(self, city_key):
        """
        Searches for location model by city key
        :key: integer with location city_key
        :return: found location model
        """
        db_model = Location.query.filter_by(city_key=city_key).first()
        return db_model
