from flask import current_app
from sqlalchemy import exc
from wsapp.models import db

class BaseRepository():
    """
    Base Repository
    """

    def add(self, model):
        """
        Saves model to database
        :model: database model
        """
        try:
            current_app.logger.info("Adding to database:{}".format(model))
            db.session.add(model)
            db.session.commit()
        except exc.IntegrityError:
            current_app.logger.error("Error while adding:{}".format(model))
            db.session.rollback()

    def delete(self, model):
        """
        Deletes model from database
        :model: database model
        """
        try:
            current_app.logger.info("Deleting from database:{}".format(model))
            db.session.delete(model)
            db.session.commit()
        except exc.IntegrityError:
            current_app.logger.error("Error while deleting from database:{}".format(model))
            db.session.rollback()
