from wsapp.repositories.base_repository import BaseRepository
from wsapp.models.report import Report

class ReportRepository(BaseRepository):
    """
    Weather Report Repository
    """
    def create_report(self, loc_model, date, temp_min, temp_max, summary):
        """
        Creates report model and saves it to database
        :loc_model: database location model
        :date: String with date in format Y-m-d
        :temp_min: Integer with minimal temperature that day
        :temp_max: Integer with maximal temperature that day
        :summary: String containing short description of weather that day
        :return: report model
        """
        db_model = Report(location=loc_model, date=date, temp_min=temp_min,
                          temp_max=temp_max, summary=summary)
        self.add(db_model)
        return db_model

    def update_report(self, report_model, new_date, new_t_min, new_t_max, new_summary):
        """
        Takes in location model and updates its fields
        :report_model: database report model to be updated
        :new_date: String with date in Y-m-d format
        :new_t_min: Integer with minimal temperature
        :new_t_max: Integer with maximal temperature
        :new_summary: String with short summary for weather that day
        """
        report_model.date = new_date
        report_model.temp_min = new_t_min
        report_model.temp_max = new_t_max
        report_model.summary = new_summary

    def find_reports_by_location(self, loc_model):
        """
        Queries database for reports that match location model provided
        :loc_model: database location model
        :return: list of found reports models for location
        """
        list_of_report_models = Report.query.filter_by(location=loc_model).all()
        return list_of_report_models

    def find_reports_by_date(self, date):
        """
        Searches for reports that match date provided
        :date: String with date in Y-m-d format
        :return: List of reports matching date
        """
        list_of_report_models = Report.query.filter_by(date=date).all()
        return list_of_report_models

    def find_reports_by_location_and_date(self, loc_model, date):
        """
        Searches for reports that match location model and date range
        :loc_model: database location model
        :start_date: string with date in Y-m-d format
        :return: list of report models matching location and date
        """
        temp = Report.query.with_parent(loc_model)
        list_of_report_models = temp.filter(Report.date == date).all()
        return list_of_report_models
