from wsapp.services import weather as WS
from wsapp.utils.location_dto import LocationDTO
from wsapp.utils.report_dto import ReportDTO

class WSController:
    """
    Weather system controler
    """

    def get_location(self, location):
        """
        Querries wsservice for location and returns location dto
        :loc_name: string with location name
        :return: location dto
        """
        loc_obj = WS.get_location_by_name(location)
        loc_dto = LocationDTO(name=loc_obj.name, key=loc_obj.city_key)
        return loc_dto

    def get_reports(self, loc_name, date_from, date_to):
        """
        Querries wsservice for reports and returns list of report dt object
        :loc_name: string with location name
        :date_from: string with date in format Y-m-d
        :date_to: string with date in format Y-m-d
        :return: list of report DT objects for given location
        """
        weather_reports = []
        reports = WS.get_reports_by_daterange_and_location(loc_name, date_from, date_to)

        for item in reports:
            weather_reports.append(ReportDTO(date=item.date, temp_max=item.temp_max,
                                             temp_min=item.temp_min, summary=item.summary))

        return weather_reports
