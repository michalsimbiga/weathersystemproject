import logging
from flask import Flask
from flask.logging import default_handler
from .config import conf_by_name
from api import init_api


def create_app(config_name='dev'):
    """
    Application factory
    :config_name: string with name of config (test, dev, prod)
    :return: flask app instance
    """
    app = Flask(__name__)
    app.config.from_object(conf_by_name[config_name])
    init_api(app)

    # conf logging
    handler = logging.FileHandler(app.config['LOGGING_LOCATION'])
    handler.setLevel(app.config['LOGGING_LEVEL'])

    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)

    from wsapp.models import db

    with app.app_context():
        from .models import location, report
        from .views import index, results, errors
        db.init_app(app)
        db.create_all()
    return app
