# Weather System Project
Provides a basic overview of weather for given location and range of dates we're interested in.

## Getting Started
First, begin by cloning this repo to your local machine:
`$ git clone https://bitbucket.org/michalsimbiga/weathersystemproject.git <YOUR_DIR_NAME>`

### Prerequisites
Before getting started, you will need to obtain an Accuweather API Key by [signing up here](http://developer.accuweather.com/).
After you obtained the Accuweather API key, in wsapp directory create a new file named acuapikey.txt and put your key there.

### Creating virtual enviroment and fetching requirements

1. Create a virtual environment using virtualenv
`$ virtualenv <your_project_name>`
    It will create a new folder in the current directory which will contain the Python executable files and a copy of pip library

    and to activate the environment we'll use
`$ source your_project_name/bin/activate`

2. Create a virtual environment using conda
`$ conda create -n yourenvname python=3.6`

    and to activate the environment we'll use
`$ activate yourenvname`

3. Install requirements
In order to install requirements, we can simply run
`$ pip install -r requirements.txt`

## Running the tests
To test the app run
`$ tox`

## Running the app
To run the app use
`$ python run.py`

# Api endpoints

|HTTP Header|Endpoint|Expected Input|Action|
|---|---|---|---|
| GET | /api/cities | None | Resolve list of cities in database |
| GET | /api/city | 'name':string| Resolve location details by name|
| POST | /api/city | 'name':string, 'city_key':integer | Create new city form input params  |
| GET | /api/report | 'name':string | Resolves reports for given location |
| POST | /api/report | 'name':string, 'date':string (Y-m-d format), 'temp_min':integer, 'temp_max':integer, 'summary':string | Create new report for location using input params  |
| GET | /api/auth | None | Provides JWT token |
